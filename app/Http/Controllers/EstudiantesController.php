<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use App\Events\EstudianteRegistrado;

class EstudiantesController extends Controller
{
    public function indice(){

    	return view('registrarEstudiante');

    }

    public function guardar( Request $request){

    	$nombre = $request->nombre;
    	$clase  = $request->clase;
    	$edad   = $request->edad;

    	$estudiante = new Student();

    	$estudiante->nombre = $nombre;
    	$estudiante->clase  = $clase;
    	$estudiante->edad   = $edad;

    	$estudiante->save();

    	$datos = array('nombre'=>$nombre, 'clase'=>$clase, 'edad'=>$edad);

    	event( new EstudianteRegistrado($datos) );

    	return 'Estudiante Registrado';

    }
}

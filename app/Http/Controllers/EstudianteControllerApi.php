<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Student;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class EstudianteControllerApi extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $student = Student::all();
        $response = Response::json($student, 200);
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $student = new Student();

        $student->nombre = $request->input('nombre');
        $student->clase = $request->input('clase');
        $student->edad = $request->input('edad');

        $student->save();

        return response()->json($student);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student = Student::find($id);

        if(!$student){
            return Response::json([

                'error' => [

                    'message' => "No se ha encontrado al estudiante."

                ]

            ], 404);
        }
        return Response::json($student, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = Student::find($id);

        $student->nombre = $request->input('nombre');
        $student->clase = $request->input('clase');
        $student->edad = $request->input('edad');

        $student->save();
        return response()->json($student);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = Student::findOrfail($id);

         if(!$student){
            return Response::json([

                'error' => [

                    'message' => "Estudiante no existente."

                ]

            ], 404);
        }

        $student->delete();

        return "Estudiante eliminado correctamente";
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function restore($id)
    {

        \App\Student::withTrashed()->find($id)->restore();

        return "Estudiante restaurado correctamente";
    }
}

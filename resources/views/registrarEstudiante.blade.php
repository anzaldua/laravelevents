<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	
	<!-- Proteccion CSRF -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Hello, world!</title>
  </head>
  <body>
    
    <div class="container bg-primary mt-5 p-5">

    	<div class="row">
    		
    		<div class="col-lg-12">
    			
				<form action="{{ route('registrarEstudiante') }}" method="POST">

					<!-- Proteccion CSRF -->
					{{ csrf_field() }}

				  <div class="form-group">

				  	<h1 class="text-center">Registro de Estudiantes</h1>

				    <label for="nombre">Nombre del estudiante</label>

				    <input type="text" class="form-control" name="nombre" placeholder="Estudiante...">

				  </div>

				  <div class="form-group">

				    <label for="clase">Nombre de la Clase</label>

				    <input type="text" class="form-control" name="clase" placeholder="Clase..">

				  </div>

				   <div class="form-group">

				    <label for="edad">Edad del Estudiante</label>

				    <input type="number" class="form-control" name="edad" placeholder="Edad..">

				  </div>

				  <button type="submit" class="btn btn-success">Submit</button>

				</form>

    		</div>

    	</div>
    </div>
    


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>